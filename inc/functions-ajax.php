<?php
// NEWS
add_action('wp_ajax_get_news_homepage', 'get_news_homepage' );
add_action('wp_ajax_nopriv_get_news_homepage', 'get_news_homepage' );

function get_news_homepage(){

	$args = [
		'post_type' 	=> 'post'
	];

	global $wp_query;
	$wp_query = new WP_Query( $args );

	if( $wp_query->have_posts() ):
		
		while( $wp_query->have_posts() ) : $wp_query->the_post();

			$listNews[] = [
								
				"titolo" => get_the_title(),
				"descrizione" => get_the_content()

			];

		endwhile;
	
	endif;

	echo json_encode($listNews);
						
    wp_die();
}

?>

