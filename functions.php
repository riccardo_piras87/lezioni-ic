<?php

	add_action('init', 'disable_wp_emojicons');
	add_action('after_setup_theme', 'tema_setup');
	add_action('wp_enqueue_scripts', 'tema_style_scripts');

	function tema_setup(){

		add_theme_support( 'html5', ['search-form']);

		// POST THUMBNAILS
		add_theme_support( 'post-thumbnails' );
		// set_post_thumbnail_size( 450, 500, true );
		add_image_size('articolo', 1200, 900, true);
		add_image_size('archivio', 400, 400, true);

		register_nav_menus([
			'primary' => 'Principale',
			'footer' => 'Footer'
		]);

	}

	function tema_style_scripts(){

		// CSS
		wp_enqueue_style( "tema", get_template_directory_uri()."/public/css/app.css");

		// JS
		wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true );
		wp_enqueue_script( 'custom', get_template_directory_uri().'/public/js/app.js', false, false, true );
		wp_localize_script( 'custom', 'ajax_object', ['ajax_url' => admin_url( 'admin-ajax.php' ) ]);

	}

	function disable_emojicons_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}

	function disable_wp_emojicons() {

		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
		add_filter( 'bp_core_fetch_avatar', function () { return ''; } );
	    add_filter( 'get_avatar', function () { return ''; } );
		add_filter( 'bp_get_signup_avatar', function () { return ''; } );
		wp_deregister_script('jquery');
		wp_deregister_script('wp-embed');

	}

	require_once 'inc/functions-ajax.php';
	

?>