import Homepage from './homepage';

var Main = (function() {
	'use strict';

	var elements = {},
		ajax__request,
		numRows;

	function init() {
		
		if($("body").hasClass("home")){
			Homepage.init();
		}
		
	}

	return {
		init:init
	};

}());

module.exports = Main;

