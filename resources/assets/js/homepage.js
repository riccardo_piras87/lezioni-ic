var Homepage = (function() {
	'use strict';

	var ajax__request;

	function bindEvents(){

		$("#bottone").on("click",function(){

			ajax__request = $.ajax({
	          	url: ajax_object.ajax_url,
				method: 'POST',
				dataType: "json",
				data: {

					action: 'get_news_homepage'

				},
				beforeSend: function(){

					$("#bottone").html("sto caricando");

					// $(".button-primary.-exportDealers").addClass("loading");
					// $(".button-primary.-exportDealers").html(window.spinLoading);

				}
			});

			ajax__request.done(function( data ){

				$("#bottone").html("BOTTONE");

				$("#slider").html('');
				for(let i=0;i<data.length;i++){

					$("#slider").append(`<div>
						<h1>${data[i].titolo}</h1>
						<p>${data[i].descrizione}</p>
					</div>`);

				}
				// $(".button-primary.-exportDealers").removeClass("loading");
				// $(".button-primary.-exportDealers").html($(".button-primary.-exportDealers").attr("data-label"));

				// if(data.status == window.onSuccess) {

				// 	alert("Esportazione inviata all'indirizzo email "+emailDest);

				// }

			});

		});

	}

	function init() {

		bindEvents();

	}

	return {
		init:init
	};

}());

module.exports = Homepage;

