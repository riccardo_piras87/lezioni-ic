<?php get_header(); ?>
<?php if ( $wp_query->have_posts() ) : ?>
	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

		<?php echo get_the_ID()." - ".get_the_title(false)." - ".get_the_date();?>

	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>

<!-- WP_QUERY->HAVE_POSTS
$dati = [
	[0],
	[1],
	[2],<-
	[3],
	[4],
	[5],
	[6]
]

[
	"ID"=>X,
	"title"=>"Titolo del post",
	"description"=>"descrizione del post"
]


for(i=0;i<10;i++){
		
	$dati[$i]["title"]

} -->